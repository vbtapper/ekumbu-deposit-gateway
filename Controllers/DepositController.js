const Boom                  = require('boom');
const async                 = require("async");
const randtoken             = require('rand-token').generator();

const moment                = require('moment');
const formatCurrency        = require('format-currency')

const config                = require('../config');
const Q                     = require('q');

var Client                  = require('node-rest-client').Client;

const Customer              = require('../Models/CustomerModel').CustomerSchema;      
const Card                  = require('../Models/CardModel').CardSchema; 
const Deposit               = require('../Models/DepositModel').DepositSchema;  
const CustomEvent           = require('../Models/CustomerEventModel').CustomerEventSchema;
const Token                 = require('../Models/TokenModel').TokenSchema;
const RechargeableCard      = require('../Models/RechargeableCardModel').RechargeableCardSchema;

function GetCardByCardNumber(cardNumber) {
    var deferred = Q.defer();
    Card.findOne({"number": cardNumber}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(err);
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function GetCustomerByID(customerID) {
    var deferred = Q.defer();
    Customer.findOne({"_id": customerID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}

function GetRechargeableCardByID(cardID) {
    var deferred = Q.defer();
    RechargeableCard.findOne({"_id": cardID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}

function CreateToken(CustID, Card, mode, ipAddress) {
    var deferred = Q.defer();

    var newTokenInfo = {
        type : 'Deposit',
        livemode : mode,
        customer_ip : ipAddress,
        card : Card._id,
        source_info: {
            source_id : CustID
        }
    }
    
    var newToken = new Token(newTokenInfo)
    newToken.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        deferred.resolve(data)
    });

    return deferred.promise;
}

function CreateDepositVendor(stationNumber, tokenID, amountParam, description) {
    var deferred = Q.defer();

    var newDepositInfo = {
        status: 'processing',
        depositid : "dep_" + randtoken.generate(20, config.deposit.depositTokenID),
        amount : amountParam,
        amountToTransfer : amountParam,
        fee: '0',
        rate: '0',
        tokenid : tokenID,
        currency : 'AOA',
        description : description,
        bankStatementDescription : 'none',
        trackingID: randtoken.generate(20, config.deposit.trackingID),
        source_info : {
            type : 'Estação Ekumbu',
            last4 : stationNumber,
            expMonth : 'none',
            expYear : 'none',
            country : 'Angola'
        }
    }

    var newDep = new Deposit(newDepositInfo) 
    newDep.save(function(err, data) {
        if(err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
} 

function CreateDepositEtopUp(topupCardIDParam, tokenID, amountParam, description) {
    var deferred = Q.defer();

    var newDepositInfo = {
        status: 'processing',
        depositid : "dep_" + randtoken.generate(20, config.deposit.depositTokenID),
        amount : amountParam,
        amountToTransfer : amountParam,
        fee: '0',
        rate: '0',
        tokenid : tokenID,
        currency : 'AOA',
        description : description,
        bankStatementDescription : 'none',
        trackingID: randtoken.generate(20, config.deposit.trackingID),
        source_info : {
            type : 'ETopUp Card',
            last4 : topupCardIDParam,
            expMonth : 'none',
            expYear : 'none',
            country : 'Angola'
        }
    }

    var newDep = new Deposit(newDepositInfo) 
    newDep.save(function(err, data) {
        if(err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
} 

function MakeEkumbuDeposit(cardToken, amountp) {
    var deferred = Q.defer();

    var client = new Client();
    var args = {
        data : {
            token : cardToken,
            amount : amountp
        },
        headers: { 
            "Content-Type": "application/json",
        }
    };
    client.post(config.vendor.url + '/deposit', args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data);
        }
        else {
            deferred.reject(data['message']);
        }
    });

    return deferred.promise;
}

function CreateCustomerEventForDepositVendor(customerID, amountA, stationNumber, dpsID) {
    var deferred = Q.defer();

    Date.prototype.monthNames = [
        "Janeiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro",
        "Octubro", "Novembro", "Dezembro"
    ];

    Date.prototype.getMonthName = function() {
        return this.monthNames[this.getMonth()];
    };
    Date.prototype.getShortMonthName = function () {
        return this.getMonthName().substr(0, 3);
    };
    var dt = new Date();

    var hours = dt.getHours();
    var mins = dt.getMinutes();
    var day = dt.getDate();
    var month = dt.getShortMonthName();

    var newCeventInfo = {
        type: "deposit",
        amount: amountA,
        customer_id: customerID,
        hour: hours + ":" + mins,
        date: month + " " + day,
        source_info : {
            name: "Estação Ekumbu" + ' #' + stationNumber,
            sourceid: stationNumber
        },
        details : {
            id : dpsID
        }
    }

    var newCustEvt = new CustomEvent(newCeventInfo)
    newCustEvt.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        deferred.resolve(data)
    });

    return deferred.promise;
}

function CreateCustomerEventForDepositETopUpCard(customerID, amountA, cardNumberParam, dpsID) {
    var deferred = Q.defer();

    Date.prototype.monthNames = [
        "Janeiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro",
        "Octubro", "Novembro", "Dezembro"
    ];

    Date.prototype.getMonthName = function() {
        return this.monthNames[this.getMonth()];
    };
    Date.prototype.getShortMonthName = function () {
        return this.getMonthName().substr(0, 3);
    };
    var dt = new Date();

    var hours = dt.getHours();
    var mins = dt.getMinutes();
    var day = dt.getDate();
    var month = dt.getShortMonthName();

    var newCeventInfo = {
        type: "deposit",
        amount: amountA,
        customer_id: customerID,
        hour: hours + ":" + mins,
        date: month + " " + day,
        source_info : {
            name: "EtopUp" + '-' + cardNumberParam.toString().substring(cardNumberParam.toString().length-4, cardNumberParam.toString().length),
            sourceid: cardNumberParam
        },
        details : {
            id : dpsID
        }
    }

    var newCustEvt = new CustomEvent(newCeventInfo)
    newCustEvt.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        deferred.resolve(data)
    });

    return deferred.promise;
}

function GetCustomerBySessionToken(token) {
    var deferred = Q.defer();
    Customer.findOne({"sessiontoken": token}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null);
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function ProcessStripeTransaction(cardNumber, cardExpMonth, cardExpYear, cardCvv, amount, custEmail, currency) {
    var deferred = Q.defer();

    var stripe = require("stripe")(config.stripe.TESTSKEY);
    stripe.tokens.create({
    card: {
        "number": cardNumber,
        "exp_month": cardExpMonth,
        "exp_year": cardExpYear,
        "cvc": cardCvv
    }
    }, function(err, token) {
        if(err) {
            deferred.reject(err);
        }
        else {
            if(token != null || token != undefined) {
                stripe.charges.create({
                    amount: (Number(amount)*100).toFixed(0),
                    currency: currency,
                    source: token['id'], // obtained with Stripe.js
                    description: "Ekumbu Deposit to customer " + custEmail
                }, function(err, charge) {
                    if(!err) {
                        if(charge['status'] == "succeeded") {
                            deferred.resolve(charge);
                        }
                        else {
                            deferred.reject(err);
                        }
                    }
                    else {
                        deferred.reject(err);
                    }
                        
                });
            }
            else {
                deferred.reject(err);
            }
        }
    });

    return deferred.promise;
}

function CreateDepositCC(amount, amountTT, description, statDesc, tokenID, stripeCharge, currencyParam, feeParam, rateParam) {
    var deferred = Q.defer();

    var newDepositInfo = {
        status: 'processing',
        depositid : "dep_" + randtoken.generate(20, config.deposit.depositTokenID),
        amount : amount,
        amountToTransfer : amountTT,
        fee: feeParam,
        rate: rateParam,
        tokenid : tokenID,
        currency : currencyParam,
        description : description,
        bankStatementDescription : statDesc,
        trackingID: randtoken.generate(20, config.deposit.trackingID),
        source_info : {
            type : stripeCharge['source'].brand,
            last4 : stripeCharge['source'].last4,
            expMonth : stripeCharge['source'].exp_month,
            expYear : stripeCharge['source'].exp_year,
            country : stripeCharge['source'].country
        }
    }

    var newDep = new Deposit(newDepositInfo) 
    newDep.save(function(err, data) {
        if(err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function CreateCustomerEvent(customerID, amountA, StripeCrg, dpsID) {
    var deferred = Q.defer();

    Date.prototype.monthNames = [
        "Janeiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro",
        "Octubro", "Novembro", "Dezembro"
    ];

    Date.prototype.getMonthName = function() {
        return this.monthNames[this.getMonth()];
    };
    Date.prototype.getShortMonthName = function () {
        return this.getMonthName().substr(0, 3);
    };
    var dt = new Date();

    var hours = dt.getHours();
    var mins = dt.getMinutes();
    var day = dt.getDate();
    var month = dt.getShortMonthName();

    var newCeventInfo = {
        type: "deposit",
        amount: amountA,
        customer_id: customerID,
        hour: hours + ":" + mins,
        date: month + " " + day,
        source_info : {
            name: StripeCrg['source'].brand + ' x-' + StripeCrg['source'].last4,
            sourceid: StripeCrg['id']
        },
        details : {
            id : dpsID
        }
    }

    var newCustEvt = new CustomEvent(newCeventInfo)
    newCustEvt.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        deferred.resolve(data)
    });

    return deferred.promise;
}

function GetCardByCustomerID(cutomerID) {
    var deferred = Q.defer();
    Card.findOne({"cardholder_info.customer_id": cutomerID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function FindDepositByTrackingID(trackingIDParam) {
    var deferred = Q.defer();
    Deposit.findOne({"trackingID": trackingIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}

module.exports = {
    DepositFromVendorRequest: function(request, reply) {
        GetCardByCardNumber(request.payload.cardn)
        .then(FoundCard => {
           GetCustomerByID(FoundCard.cardholder_info.customer_id)
            .then(FoundCustomer => {
                CreateToken(FoundCustomer._id, FoundCard, request.payload.livemode, request.payload.ipaddress)
                .then(CreatedToken => {
                    CreateDepositVendor(request.payload.stationNumber, CreatedToken._id, request.payload.amount, request.payload.description)
                    .then(DepositCreated => {
                        CreatedToken.deposit_info.deposit_id = DepositCreated['depositid'];
                        CreatedToken.used = true;
                        CreatedToken.save();

                        let opts = {
                            format : '%v %c', 
                            code : 'AOA'
                        }
                        var formatedAmount = formatCurrency(request.payload.amount, opts)
                        CreateCustomerEventForDepositVendor(FoundCustomer._id, formatedAmount, request.payload.stationNumber, DepositCreated.depositid)
                        .then(CustomerEventCreated => {
                            MakeEkumbuDeposit(FoundCard.security.secret_token, request.payload.amount.toString())
                            .then(AccountDeposit => {
                                rspInfo = {
                                    statusCode: 200,
                                    error: null,
                                    message: 'success',
                                    trackingID: DepositCreated['trackingID']
                                }
                                return reply(rspInfo)
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            rspInfo = {
                                statusCode: 500,
                                error: 'Internal Error',
                                message: 'Problems creating customer event ' + err
                            }
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        rspInfo = {
                            statusCode: 500,
                            error: 'Internal Error',
                            message: 'Problem creating the deposit ' + err
                        }
                    })
                })
                .catch(err => {
                    console.log(err)
                    rspInfo = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'Problem creating the token ' + err
                    }
                    return reply(rspInfo);
                })
            })
            .catch(err => {
                console.log(err)
                rspInfo = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Problem finding the customer ' + err
                }
                return reply(rspInfo);
            })
        })
        .catch(err => {
            console.log(err)
            var rspInfo = {
                statusCode: 400,
                error: 'Not Found',
                message: 'Card not found'
            }
            return reply(rspInfo)
        })
    },

    DepositFromCreditCard : function(request, reply) {
        GetCustomerBySessionToken(request.payload.sessiontoken)
        .then(FoundCustomer => {
            GetCardByCustomerID(FoundCustomer._id)
            .then(FoundCard => {
                if(FoundCard.source == undefined || FoundCard.source == null) {
                    rspMessage = {
                        statusCode : 403,
                        error : 'Request Error',
                        message : 'There is not ekumbu account setup'
                    }
                    return reply(rspMessage);
                }
                else {
                    ProcessStripeTransaction(request.payload.cardNumber, request.payload.expmonth, request.payload.expyear, request.payload.cvv, request.payload.amount, FoundCustomer.email, request.payload.currency)
                    .then(StripeCharge => {
                        CreateToken(FoundCustomer._id, FoundCard, request.payload.livemode, request.payload.ipaddress)
                        .then(TokenCreated => {
                            CreateDepositCC(request.payload.amount, request.payload.amouttransfer, request.payload.description, request.payload.statementdescription, TokenCreated['_id'], StripeCharge, request.payload.currency, request.payload.fee, request.payload.rate)
                            .then(NewDeposit => {
                                
                                TokenCreated.deposit_info.deposit_id = NewDeposit['depositid'];
                                TokenCreated.used = true;
                                TokenCreated.save();

                                CreateCustomerEvent(FoundCustomer._id, request.payload.amouttransfer, StripeCharge, NewDeposit.depositid)
                                .then(CustomerEvent => {
                                    // TODO send an email to the customer, we are processing
                                    MakeEkumbuDeposit(FoundCard.security.secret_token, request.payload.amountToEkb)
                                    .then(ekbDeposit => {
                                        rspMessage = {
                                            statusCode : 200,
                                            error : null,
                                            message : 'success',
                                            trackingID: NewDeposit['trackingID']
                                        }
                                        return reply(rspMessage);
                                    })
                                    .catch(err => {
                                        console.log(err)
                                        rspMessage = {
                                            statusCode : 500,
                                            error : 'Internal Error',
                                            message : 'Internal Error Transfer to your Ekumbu, but the charge was made'
                                        }
                                        return reply(rspMessage);
                                    })
                                })
                                .catch(err => {
                                    console.log(err)
                                    rspMessage = {
                                        statusCode : 500,
                                        error : 'Internal Error',
                                        message : 'Internal Error Creating Deposit, but the charge was made'
                                    }
                                    return reply(rspMessage);
                                })
                            })
                            .catch(err => {
                                // TODO send an email to the Customer saying the charge was made but an internal error occured we are fixing ...
                                // Send an email to admin to fix the problem
                                console.log(err)
                                rspMessage = {
                                    statusCode : 500,
                                    error : 'Internal Error',
                                    message : 'Internal Error Creating Deposit, but the charge was made'
                                }
                                return reply(rspMessage);
                            })
                        })
                        .catch(err => {
                            // TODO send an email to the Customer saying the charge was made but an internal error occured we are fixing ...
                            // Send An email to Admin to fix the problem
                            console.log(err)
                            rspMessage = {
                                statusCode : 500,
                                error : 'Internal Error',
                                message : 'Internal Error Creating Token, but the charge was made'
                            }
                            return reply(rspMessage);
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        rspMessage = {
                            statusCode : 403,
                            error : 'Request Error',
                            message : 'Invalid Card or Insuficient Funds'
                        }
                        return reply(rspMessage);
                    })
                }
            })
            .catch(err => {
                console.log(err)
                rspMessage = {
                    statusCode : 403,
                    error : 'Request Error',
                    message : 'Customer Invalid Card or Card Expired'
                }
                return reply(rspMessage);
            })
        })
        .catch(err => {
            console.log(err)
            rspMessage = {
                statusCode : 403,
                error : 'Request Error',
                message : 'Invalid Customer Session Token'
            }
            return reply(rspMessage);
        })
    },

    ConfirmDeposit: function(request, reply) {
        // curl --request PATCH -i http://deposit.ekumbu.com/confirmdf -H "Authorization: Bearer deposit-access-token Content-Type: application/json charset=UTF-8" -d "trackingID=as4asddF5jdFd5A2G42c"
        FindDepositByTrackingID(request.payload.trackingID)
        .then(FoundDeposit => {
            if(FoundDeposit.status === 'confirmed') {
                var infoResponse = {
                    statusCode: 403,
                    error : 'Request Error',
                    message: 'this deposit was confirmed already'
                }
                return reply(infoResponse)
            }
            else {
                FoundDeposit.status = 'confirmed';
                FoundDeposit.save();
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success'
                }
                return reply(infoResponse);
            }
        })
        .catch(err => {
            console.log(err)
            var infoResponse = {
                statusCode: 404,
                error: 'Not Found',
                message: 'deposit not found check the logs for details'
            }
            return reply(infoResponse);
        })
    },

    DepositFromEtopUpRequest: function(request, reply) {
        GetCardByCardNumber(request.payload.cardn)
        .then(FoundCard => {
           GetCustomerByID(FoundCard.cardholder_info.customer_id)
            .then(FoundCustomer => {
                CreateToken(FoundCustomer._id, FoundCard, request.payload.livemode, request.payload.ipaddress)
                .then(CreatedToken => {
                    CreateDepositEtopUp(request.payload.etopupcardnumber, CreatedToken._id, request.payload.amount, request.payload.description)
                    .then(DepositCreated => {
                        CreatedToken.deposit_info.deposit_id = DepositCreated['depositid'];
                        CreatedToken.used = true;
                        CreatedToken.save();

                        let opts = {
                            format : '%v %c', 
                            code : 'AOA'
                        }
                        var formatedAmount = formatCurrency(request.payload.amount, opts)
                        CreateCustomerEventForDepositETopUpCard(FoundCustomer._id, formatedAmount, request.payload.etopupcardnumber, DepositCreated.depositid)
                        .then(CustomerEventCreated => {
                            MakeEkumbuDeposit(FoundCard.security.secret_token, request.payload.amount.toString())
                            .then(AccountDeposit => {
                                GetRechargeableCardByID(request.payload.topupid)
                                .then(FoundRechargeableCard => {
                                    
                                    FoundRechargeableCard.status = true;
                                    FoundRechargeableCard.dateUsed = new Date();
                                    FoundRechargeableCard.clientInfo = {
                                        name: FoundCustomer.firstname + ' ' + FoundCustomer.lastname,
                                        email: FoundCustomer.email,
                                        id: FoundCustomer._id,
                                        cardlast4: FoundCard.number
                                    }
                                    
                                    FoundRechargeableCard.save();

                                    rspInfo = {
                                        statusCode: 200,
                                        error: null,
                                        message: 'success',
                                        trackingID: DepositCreated['trackingID']
                                    }
                                    return reply(rspInfo)
                                })
                                .catch(error => {
                                    console.log(error)
                                    var rspInfo = {
                                        statusCode: 500,
                                        error: 'Internal Error',
                                        message: 'Problems creating customer event ' + err
                                    }
                                    return reply(rspInfo)
                                })
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            var rspInfo = {
                                statusCode: 500,
                                error: 'Internal Error',
                                message: 'Problems creating customer event ' + err
                            }
                            return reply(rspInfo)
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        var rspInfo = {
                            statusCode: 500,
                            error: 'Internal Error',
                            message: 'Problem creating the deposit ' + err
                        }
                        return reply(rspInfo)
                    })
                })
                .catch(err => {
                    console.log(err)
                    rspInfo = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'Problem creating the token ' + err
                    }
                    return reply(rspInfo);
                })
            })
            .catch(err => {
                console.log(err)
                rspInfo = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Problem finding the customer ' + err
                }
                return reply(rspInfo);
            })
        })
        .catch(err => {
            console.log(err)
            var rspInfo = {
                statusCode: 400,
                error: 'Not Found',
                message: 'Card not found'
            }
            return reply(rspInfo)
        })
    },
}