const DepositController                         = require('./Controllers/DepositController')

const Joi               = require('joi');

module.exports = [{
    method: 'POST',
    path: '/dfvendor',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: 'SuperAdmin'
        },
        handler: DepositController.DepositFromVendorRequest,
        validate: {
            payload: {
                cardn : Joi.string().min(16).max(16),
                amount : Joi.number().required(),
                livemode: Joi.string().min(4).max(5),
                description : Joi.string(),
                ipaddress : Joi.string(),
                stationNumber: Joi.number()
            }
        }
    }
},
{
    method: 'POST',
    path: '/dfcard',
    config : {
        auth: {
            strategy: 'ekumbu-manager',
            scope: 'SuperAdmin'
        },
        handler : DepositController.DepositFromCreditCard,
        validate : {
            payload : {
                cardNumber : Joi.string().min(16).max(16),
                expmonth : Joi.string(),
                expyear : Joi.string(),
                cvv : Joi.string().min(3).max(3),
                sessiontoken : Joi.string().min(20).max(20),
                amount : Joi.number(),
                amouttransfer : Joi.string(),
                livemode: Joi.string().min(4).max(5),
                ipaddress : Joi.string(),
                description : Joi.string(),
                statementdescription : Joi.string(),
                currency : Joi.string(),
                amountToEkb : Joi.string(),
                rate: Joi.string(),
                fee: Joi.string()
            }
        }
    }
},
{
    method: 'PATCH',
    path: '/confirmdf',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: 'SuperAdmin'
        },
        handler: DepositController.ConfirmDeposit,
        validate: {
            payload: {
                trackingID: Joi.string().required()
            }
        }
    }
},
{
    method: 'POST',
    path: '/dfetopup',
    config: {
        handler: DepositController.DepositFromEtopUpRequest,
        validate: {
            payload: {
                cardn : Joi.string().min(16).max(16),
                amount : Joi.number().required(),
                description : Joi.string(),
                ipaddress : Joi.string(),
                topupid: Joi.string().required(),
                etopupcardnumber: Joi.number().required()
            }
        }
    }
}
]