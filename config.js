const STRIPETESTKEY                 = process.env.STRIPETESTKEY;
const STRIPELIVEKEY                 = process.env.STRIPELIVEKEY;

module.exports = {
    Server: {
        Port: 3013
    },
    manager : {
        superAdmin: {
            scope: ['SuperAdmin'],
            superManagerToken: 'deposit-access-token'
        }
    },
    vendor : {
        url : 'http://stagingapivendor.ekumbu.com'
    },
    deposit: {
        depositTokenID : "435324dsfsd32423SFDSFFDS23432423DSXF32432SDSDWSADSADasdasr32423sdadSDFDSF",
        trackingID : "6adkA453kuA4wydhsadB4dfasj2SDFdcsdkd34jadsgAWGDJV43422scsd2234cndLPCmxapzqDSF453weasdDAd",
        fee: 6
    },
    stripe : {
        TESTSKEY : STRIPETESTKEY || 'sk_test_AzVP5gfs2tLNZAjt5ERvGZ31',
        LIVESKEY : STRIPELIVEKEY || 'sk_live_uK6XxRZqb20FAWNU95b6IaOK' 
    },
}