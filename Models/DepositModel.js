const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
var db                      = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');

const DepositSchema = new Schema({
    created : { type: Date, default: Date.now },
    status : { type : String, required: true },
    depositid: { type: String, required: true, unique: true },
    amount: { type : Number, required: true },
    currency: {type: String, required: true},
    amountToTransfer : {type : String, required: true},
    fee : { type: String },
    rate : {type: String},
    tokenid : { type: String, required: true },
    description : { type: String },
    bankStatementDescription : {type: String},
    trackingID: {type: String, required: true},
    source_info : {
        type : {type: String, required: true},
        last4 : {type: String, required: true},
        expMonth : {type: String, required: true},
        expYear : {type: String, required: true},
        country : {type: String, required: true}
    }
});

var deposit = Mongoose.model('deposits', DepositSchema);
module.exports = {
    DepositSchema: deposit
};