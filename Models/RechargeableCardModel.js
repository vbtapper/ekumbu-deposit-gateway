const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
var db                      = require('../MongoManager/MongoConnection');

const RechargeableCardSchema = new Schema({
    created : { type: Date, default: Date.now },
    status : {type : Boolean, default: false},
    number : {type: String, required: true},
    amount: {type: Number, required: true},
    dateUsed: {type: Date,},
    currency : { type : String, required: true, default: "AOA"},
    confirmationCode : {type: String},
    clientInfo : {
        name: {type: String},
        email: {type: String},
        id: {type: String},
        cardlast4: {type: String}
    }
});

RechargeableCardSchema.pre('save', function(next) {
    next();
});

var rechargeablecard = Mongoose.model('rechargeablecard', RechargeableCardSchema);
module.exports = {
    RechargeableCardSchema: rechargeablecard
};